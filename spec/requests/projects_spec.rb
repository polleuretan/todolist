require 'spec_helper'

describe 'Projects' do
  let(:project_attributes) { ['tasks', 'title', 'user_id', 'created_at', 'updated_at', 'id'] }
  describe 'GET /projects' do
    context 'when not logged in' do
      it 'should reject with 401' do
        get projects_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
        @owned_projects = []
        2.times do
          @owned_projects << create(:project, :user => @user)
        end
      end
      it 'should return json array of projects with allowed attributes' do
        get projects_path, nil, jsonHeaders
        status.should be(200)
        jsonBody.should be_kind_of(Array)
        jsonBody.each do |project|
          (project.keys.sort).should eql(project_attributes.sort)
        end
      end
      it 'should return json array of projects with tasks' do
        @owned_projects.each do |project|
          2.times do
            create(:task, :project => project)
          end
        end
        get projects_path, nil, jsonHeaders
        status.should be(200)
        jsonBody.should be_kind_of(Array)
        jsonBody.each do |project|
          project.keys.should include('tasks')
          project['tasks'].should be_kind_of(Array)
          project['tasks'].count.should eql(2)
        end
      end
      it 'should return only user`s own projects' do
        other_project = create(:project)
        get projects_path, nil, jsonHeaders
        status.should be(200)
        jsonBody.find do |project|
          project['id'] == other_project.id
        end.should be_nil
      end
    end
  end

  describe 'GET /projects/:id' do
    context 'when not logged in' do
      it 'should reject with 401' do
        project = create(:project)
        get "projects/#{project.id}", nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
        @owned_project = create(:project, :user => @user)
      end
      it 'should return json representation of project with allowed attributes' do
        get "projects/#{@owned_project.id}", nil, jsonHeaders
        status.should be(200)
        jsonBody.should be_kind_of(Hash)
        (jsonBody.keys.sort).should eql(project_attributes.sort)
      end
      it 'should return 404 for project that is not exists' do
        get "projects/#{@owned_project.id + 1}", nil, jsonHeaders
        status.should be(404)
      end
      it 'should return 404 for projects not owned by user' do
        other_project = create(:project)
        get "projects/#{other_project.id}", nil, jsonHeaders
        status.should be(404)
      end
    end
  end

  describe 'POST /project' do
    context 'when not logged in' do
      it 'should reject with 401' do
        get projects_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
      end
      it 'should create project and respond with its json representation(201)' do
        -> do
          post projects_path, attributes_for(:project), jsonHeaders
        end.should change(Project, :count).by(1)
        status.should be(201)
        jsonBody.should be_kind_of(Hash)
        (jsonBody.keys.sort).should eql(project_attributes.sort)
      end
      it 'should set current_user as new project`s owner' do
        post projects_path, attributes_for(:project), jsonHeaders
        Project.find_by_id(jsonBody['id']).user.id.should eql(@user.id)
      end
      it 'should not create new project with invalid attributes(422)' do
        -> do
          post projects_path, attributes_for(:project, :title => ''), jsonHeaders
        end.should_not change(Project, :count)

        status.should be(422)
      end
    end
  end

  describe 'PUT /project' do
    context 'when not logged in' do
      it 'should reject with 401' do
        get projects_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
        @owned_project = create(:project, :user => @user)
      end
      it 'should update project with valid attributes(204)' do
        -> do
          put "projects/#{@owned_project.id}", attributes_for(:project, :title => 'newTitle'), jsonHeaders
          @owned_project.reload
        end.should change(@owned_project, :title).to('newTitle')

        status.should be(204)
      end
      it 'should not update project with invalid attributes(422)' do
        -> do
          put "projects/#{@owned_project.id}", attributes_for(:project, :title => ''), jsonHeaders
          @owned_project.reload
        end.should_not change(@owned_project, :title)

        status.should be(422)
      end
      it 'should not update project that user doesnt own(404)' do
        other_project = create(:project)
        -> do
          put "projects/#{other_project.id}", attributes_for(:project, :title => 'newTitle'), jsonHeaders
          other_project.reload
        end.should_not change(other_project, :title)

        status.should be(404)
      end
      it 'should return 404 for project that is not exists' do
        put "projects/#{@owned_project.id + 1}", nil, jsonHeaders
        status.should be(404)
      end
    end
  end

  describe 'DELETE /project' do
    context 'when not logged in' do
      it 'should reject with 401' do
        get projects_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
        @owned_project = create(:project, :user => @user)
      end
      it 'should delete project owned by user(204)' do
        -> do
          delete "projects/#{@owned_project.id}", nil, jsonHeaders
        end.should change(Project, :count).by(-1)
        Project.find_by_id(@owned_project.id).should be_nil
        
        status.should be(204)
      end
      it 'should not delete project that user doesnt own(404)' do
        other_project = create(:project)
        -> do
          delete "projects/#{other_project.id}", nil, jsonHeaders
        end.should_not change(Project, :count)

        status.should be(404)
      end
      it 'should return 404 for project that is not exists' do
        delete "projects/#{@owned_project.id + 1}", nil, jsonHeaders
        status.should be(404)
      end
    end
  end
end
