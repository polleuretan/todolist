require 'spec_helper'

describe 'Sessions' do
  describe 'POST /login' do
    context 'when not logged in' do
      it 'should login valid user' do
        user = create(:user)
        post login_path, {:email => user.email, :password => user.password}, jsonHeaders
        session[:user_id].should eql(user.id)
        response.status.should be(200)
      end
      it 'should refuse to login invalid user' do
        user = build(:user)
        post login_path, {:email => user.email, :password => user.password}, jsonHeaders
        session[:user_id].should be_nil
        response.status.should be(422)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
      end
      it 'should reject with 400' do
        post login_path, {:email => @user.email, :password => @user.password}, jsonHeaders
        session[:user_id].should eql(@user.id)
        status.should be(400)
      end
    end
  end

  describe 'POST /logout' do
    context 'when not logged in' do
      it 'should reject with 401' do
        post logout_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
      end
      it 'should logout user' do
        post logout_path, nil, jsonHeaders
        session[:user_id].should be_nil
        status.should be(200)
      end
    end
  end

  describe 'GET /authenticated' do
    context 'when not logged in' do
      it 'should reject with 401' do
        get authenticated_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
      end
      it 'should return user' do
        get authenticated_path, nil, jsonHeaders
        jsonBody['id'].should eql(@user.id)
        status.should be(200)
      end
    end
  end
end
