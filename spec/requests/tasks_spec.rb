require 'spec_helper'

describe "Tasks" do
  let(:task_attributes) { ['id', 'title', 'project_id', 'priority', 'deadline', 'done', 'created_at', 'updated_at'] }
  describe 'POST /projects/{id}/task' do
    context 'when not logged in' do
      it 'should reject with 401' do
        get projects_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
        @owned_project = create(:project, :user => @user)
      end
      it 'should create task and respond with its json representation(201)' do
        -> do
          post "/projects/#{@owned_project.id}/tasks", attributes_for(:task), jsonHeaders
        end.should change(Task, :count).by(1)
        status.should be(201)
        jsonBody.should be_kind_of(Hash)
        (jsonBody.keys.sort).should eql(task_attributes.sort)
      end
      it 'should set current_user as new task`s owner' do
        post "/projects/#{@owned_project.id}/tasks", attributes_for(:task), jsonHeaders
        Task.find_by_id(jsonBody['id']).project.id.should eql(@owned_project.id)
      end
      it 'should not create new task with invalid attributes(422)' do
        -> do
          post "/projects/#{@owned_project.id}/tasks", attributes_for(:task, :title => ''), jsonHeaders
        end.should_not change(Task, :count)
        status.should be(422)
      end
      it 'should return 404 for projects not owned by user' do
        other_project = create(:project)
        post "/projects/#{other_project.id}/tasks", attributes_for(:task), jsonHeaders
        status.should be(404)
      end
    end
  end

  describe 'PUT /tasks/{id}' do
    context 'when not logged in' do
      it 'should reject with 401' do
        get projects_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
        @owned_project = create(:project, :user => @user)
        @owned_task = create(:task, :project => @owned_project)
      end
      it 'should update task with valid attributes(204)' do
        -> do
          put "tasks/#{@owned_task.id}", attributes_for(:task, :title => 'newTitle'), jsonHeaders
          @owned_task.reload
        end.should change(@owned_task, :title).to('newTitle')
        status.should be(204)
      end
      it 'should not update task with invalid attributes(422)' do
        -> do
          put "tasks/#{@owned_task.id}", attributes_for(:task, :title => ''), jsonHeaders
          @owned_task.reload
        end.should_not change(@owned_task, :title)
        status.should be(422)
      end
      it 'should not update task that user doesnt own(404)' do
        other_project = create(:project)
        other_task = create(:task, :project => other_project)
        -> do
          put "tasks/#{other_task.id}", attributes_for(:task, :title => 'newTitle'), jsonHeaders
          other_task.reload
        end.should_not change(other_task, :title)
        status.should be(404)
      end
      it 'should return 404 for task that is not exists' do
        put "tasks/#{@owned_task.id + 1}", nil, jsonHeaders
        status.should be(404)
      end
    end
  end

  describe 'DELETE /tasks/{id}' do
    context 'when not logged in' do
      it 'should reject with 401' do
        get projects_path, nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
        @owned_project = create(:project, :user => @user)
        @owned_task = create(:task, :project => @owned_project)
      end
      it 'should delete task owned by user(204)' do
        -> do
          delete "tasks/#{@owned_task.id}", nil, jsonHeaders
        end.should change(Task, :count).by(-1)
        Task.find_by_id(@owned_task.id).should be_nil
        status.should be(204)
      end
      it 'should not delete task that user doesnt own(404)' do
        other_project = create(:project)
        other_task = create(:task, :project => other_project)
        -> do
          delete "tasks/#{other_task.id}", nil, jsonHeaders
        end.should_not change(Task, :count)

        status.should be(404)
      end
      it 'should return 404 for task that is not exists' do
        delete "tasks/#{@owned_task.id + 1}", nil, jsonHeaders
        status.should be(404)
      end
    end
  end

  describe 'GET /tasks/:id' do
    context 'when not logged in' do
      it 'should reject with 401' do
        task = create(:task)
        get "tasks/#{task.id}", nil, jsonHeaders
        status.should be(401)
      end
    end
    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
        @owned_project = create(:project, :user => @user)
        @owned_task = create(:task, :project => @owned_project)
      end
      it 'should return json representation of task with allowed attributes' do
        get "tasks/#{@owned_task.id}", nil, jsonHeaders
        status.should be(200)
        jsonBody.should be_kind_of(Hash)
        (jsonBody.keys.sort).should eql(task_attributes.sort)
      end
      it 'should return 404 for task that is not exists' do
        get "tasks/#{@owned_task.id + 1}", nil, jsonHeaders
        status.should be(404)
      end
      it 'should return 404 for tasks not owned by user' do
        other_project = create(:project)
        other_task = create(:task, :project => other_project)
        get "tasks/#{other_task.id}", nil, jsonHeaders
        status.should be(404)
      end
    end
  end
end
