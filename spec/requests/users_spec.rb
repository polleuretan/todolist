require 'spec_helper'

describe 'Users' do
  describe 'POST /users' do
    context 'when not logged in' do
      it 'creates new user with valid attributes(201)' do
        -> do
          post users_path, attributes_for(:user), jsonHeaders
        end.should change(User, :count).by(1)

        response.status.should be(201)
      end
      it 'signs user in after creation' do
        post users_path, attributes_for(:user), jsonHeaders
        session[:user_id].should eql(jsonBody['id'])
      end
      it 'does not create new user with invalid attributes(422)' do
        -> do
          post users_path, attributes_for(:user, :email => ''), jsonHeaders
        end.should_not change(User, :count)

        response.status.should be(422)
      end
    end

    context 'when logged in' do
      before do
        @user = create(:user)
        login(@user)
      end
      it 'should reject with 400' do
        post users_path, nil, jsonHeaders
        status.should be(400)
      end
    end
  end
end
