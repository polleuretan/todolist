require 'spec_helper'

describe Task do
  it 'sould create task with valid attributes' do
    lambda{ create(:task) }.should change(Task, :count).by(1)
  end

  it 'should require title' do
    task = build(:task, :title => '');
    task.should_not be_valid
    task.errors.should include(:title)
  end

  it 'should require project' do
    task = build(:task, :project => nil);
    task.should_not be_valid
    task.errors.should include(:project)
  end

  it 'should require priority' do
    task = build(:task, :priority => nil);
    task.should_not be_valid
    task.errors.should include(:priority)
  end

  it 'should have default priority' do
    task = build(:task);
    task.priority.should eq('normal')
  end

  it 'should convert priority on get' do
    Task::PRIORITIES.each_with_index do |name, index|
      task = build(:task, :priority => index);
      task.priority.should eql(name)
    end
  end

  it 'should accept valid priority' do
    Task::PRIORITIES.each do |name|
      task = build(:task, :priority => name);
      task.should be_valid
    end
    Task::PRIORITIES.count.times do |name|
      task = build(:task, :priority => name);
      task.should be_valid
    end
  end

  it 'should reject invalid priority' do
    task = build(:task, :priority => 'invalid');
    task.should_not be_valid

    task = build(:task, :priority => Task::PRIORITIES.count + 1);
    task.should_not be_valid
  end

  it 'should accept valid deadline as datetime in format "YYYY-mm-dd HH-MM-SS"' do
      task = build(:task, :deadline => '2001-02-03 04:05:06');
      task.should be_valid
  end

  it 'should not accept invalid deadline' do
      task = build(:task, :deadline => '2001-02-03 04:05:96');
      task.deadline.should be_nil
  end

  describe 'ownage' do
    before do
      @user = create(:user)
      owned_project = create(:project, :user => @user)
      @owned_task = create(:task, :project => owned_project)
      other_project = create(:project)
      @other_task = create(:task, :project => other_project)
    end

    it 'should return true for owner and false otherwise' do
      @owned_task.should be_owned_by(@user)
      @other_task.should_not be_owned_by(@user)
    end
  end
end
