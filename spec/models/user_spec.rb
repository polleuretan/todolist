require 'spec_helper'

describe User do
  it 'sould create user with valid attributes' do
    lambda{ create(:user) }.should change(User, :count).by(1)
  end

  it 'should require email' do
    user = build(:user, :email => '');
    user.should_not be_valid
    user.errors.should include(:email)
  end
  it 'should require unique email' do
    user1 = create(:user);
    user2 = build(:user, :email => user1.email.upcase);
    user2.should_not be_valid
    user2.errors.should include(:email)
  end

  it 'should validate email format' do
    user1 = build(:user, :email => 'invalid_email');
    user1.should_not be_valid
    user1.errors.should include(:email)
  end

  it 'should require password' do
    user = build(:user, :password => '');
    user.should_not be_valid
    user.errors.should include(:password)
  end

  it 'should require password_confirmation' do
    user = build(:user, :password_confirmation => '');
    user.should_not be_valid
    user.errors.should include(:password_confirmation)
  end
  it 'password should match password_confirmation' do
    user = build(:user, :password_confirmation => 'invalid');
    user.should_not be_valid
    user.errors.should include(:password_confirmation)
  end
end
