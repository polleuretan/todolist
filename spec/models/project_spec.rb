require 'spec_helper'

describe Project do

  it 'sould create project with valid attributes' do
    lambda{ create(:project) }.should change(Project, :count).by(1)
  end

  it 'should require title' do
    project = build(:project, :title => '');
    project.should_not be_valid
    project.errors.should include(:title)
  end

  it 'should require user' do
    project = build(:project, :user => nil);
    project.should_not be_valid
    project.errors.should include(:user)
  end

  it 'should destroy dependent tasks' do
    project = create(:project);
    create(:task, :project => project);
    lambda{ Project.destroy(project.id) }.should change(Task, :count).by(-1)
  end

  describe 'ownage' do
    before do
      @user = create(:user)
      @owned_project = create(:project, :user => @user)
      @other_project = create(:project)
    end

    it 'should return user`s projects' do
      result = Project.owned_by(@user)
      result.should include(@owned_project)
      result.should_not include(@other_project)
    end

    it 'should return true for owner and false otherwise' do
      @owned_project.should be_owned_by(@user)
      @other_project.should_not be_owned_by(@user)
    end
  end
end