FactoryGirl.define do
  factory :project do |f|
    user
    f.sequence(:title) { |n| "Project#{n}" }
  end

  factory :task do |f|
    project
    f.sequence(:title) { |n| "Task#{n}" }
  end

  factory :user do |f|
    f.sequence(:email) { |n| "user#{n}@example.com" }
    password 'password'
    password_confirmation 'password'
  end
end