module RequestHelpers
  def jsonHeaders
    {
      'HTTP_ACCEPT' => 'application/json',
    }
  end
  def login(user)
    post login_path, {:email => user.email, :password => user.password}, jsonHeaders
  end
  def jsonBody
    @jsonBody ||= JSON.parse(response.body)
  end
end