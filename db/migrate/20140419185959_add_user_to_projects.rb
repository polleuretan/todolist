class AddUserToProjects < ActiveRecord::Migration
  def change
    change_table :projects do |t|
      t.references :user
    end
  end
end
