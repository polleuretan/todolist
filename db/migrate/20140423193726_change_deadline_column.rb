class ChangeDeadlineColumn < ActiveRecord::Migration
  def up
    change_column :tasks, :deadline, :datetime
  end
end
