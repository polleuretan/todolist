class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.references :project, index: true
      t.integer :priority, default: 1
      t.datetime :deadline
      t.boolean :done, default: false

      t.timestamps
    end
  end
end
