class Task < ActiveRecord::Base
  belongs_to :project
  PRIORITIES = %w(low normal high)

  validates_presence_of :title, :project, :priority
  validates :priority, :numericality => {
    :only_integer => true,
    :less_than => Task::PRIORITIES.count,
    :message => 'Invalid priority'
  }

  def owned_by?(user)
    project.user_id == user.id
  end

  def priority=(val)
    if Task::PRIORITIES.include?(val)
      super(Task::PRIORITIES.index(val))
    elsif self.valid? || self.errors[:priority].blank?
      super(val)
    else
      super(nil)
    end
  end

  def priority
    priority = super
    Task::PRIORITIES[priority] unless priority.nil?
  end

  def deadline=(val)
    super
  rescue ArgumentError
    super(nil)
  end
end
