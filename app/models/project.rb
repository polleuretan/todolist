class Project < ActiveRecord::Base
  has_many :tasks, :dependent => :destroy
  belongs_to :user

  validates_presence_of :title, :user

  def self.owned_by(user)
    where("user_id=?", user.id)
  end

  def owned_by?(user)
    user_id == user.id
  end
end
