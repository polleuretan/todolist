angular.module('todoApp.controllers', [])
  .controller('TasksIndexCtrl', ['$scope', 'Tasks', 'Notifications', function ($scope, Tasks, Notifications){
    $scope.priorities = Tasks.priorities;
    $scope.task = new Tasks({});
    $scope.save = function (project){
      $scope.task.$save({project_id: project.id}).then(function (){
        Notifications.push('Task created', 'success');
        project.tasks.push($scope.task);
        $scope.task = new Tasks({});
      }, function (){
        Notifications.push('Task creation failed', 'danger');
      });
    };
    $scope.delete = function (project, task){
      Tasks.delete({id: task.id}).$promise.then(function (){
        Notifications.push('Task deleted', 'success');
        var index = project.tasks.indexOf(task);
        project.tasks.splice(index, 1);
      }, function (){
        Notifications.push('Task deletion failed');
      });
    };
  }])
  .controller('TasksDynamicUpdateCtrl', ['Debounce', '$scope', 'Tasks', function (Debounce, $scope, Tasks){
    var debounce = new Debounce();
    $scope.hoverControls = function (project){
      project.showControls = !project.showControls;
    };
    $scope.update = function (task){
      debounce.debounce(function (){
        Tasks.update({id: task.id}, task);
      }, 500);
    };
  }])
  .controller('TasksUpdateCtrl', ['$location', '$scope', '$routeParams', 'Tasks', 'Notifications', function ($location, $scope, $routeParams, Tasks, Notifications){
    $scope.priorities = Tasks.priorities;
    $scope.task = Tasks.get({id: $routeParams.id});
    $scope.save = function (){
      $scope.task.$update().then(function (){
        Notifications.push('Task updated', 'success');
        $location.path('/');
      }, function (){
        Notifications.push('Task update failed', 'danger');
      });
    };
  }])
  .controller('ProjectsIndexCtrl', ['$scope', 'Projects', 'Notifications', function ($scope, Projects, Notifications){
    $scope.hoverControls = function (project){
      project.showControls = !project.showControls;
    };
    $scope.projects = Projects.query();
    $scope.delete = function (project){
      project.$delete().then(function (){
        Notifications.push('Project deleted', 'success');
        var index = $scope.projects.indexOf(project);
        $scope.projects.splice(index, 1);
      }, function (){
        Notifications.push('Project deletion failed', 'danger');
      });
    };
  }])
  .controller('ProjectsUpdateCtrl', ['$location', '$scope', '$routeParams', 'Projects', 'Notifications', function ($location, $scope, $routeParams, Projects, Notifications){
    $scope.project = Projects.get({id: $routeParams.id});
    $scope.save = function (){
      $scope.project.$update().then(function (){
        Notifications.push('Project updated', 'success');
        $location.path('/');
      }, function (){
        Notifications.push('Project update failed', 'danger');
      });
    };
  }])
  .controller('ProjectsCreateCtrl', ['$location', '$scope', '$routeParams', 'Projects', 'Notifications', function ($location, $scope, $routeParams, Projects, Notifications){
    $scope.project = new Projects({});
    $scope.save = function (){
      $scope.project.$save().then(function (){
        Notifications.push('Project created', 'success');
        $location.path('/');
      }, function (){
        Notifications.push('Project creation failed', 'danger');
      });
    };
  }])
  .controller('MainCtrl',
    ['$location', '$scope', 'Auth', 'Notifications', function ($location, $scope, Auth, Notifications){
    $scope.notifications = Notifications.data;
    $scope.logout = function (){
      Auth.logout().then(function (){
        $location.path('/login');
      }, function (){
        $location.path('/');
      });
    };
    $scope.isLoggedIn = function (){
      return Auth.isLoggedIn();
    };
  }])
  .controller('LoginCtrl',
    ['$location', '$scope', 'Auth', 'Notifications', function ($location, $scope, Auth, Notifications){
      $scope.errorMsg = '';
      $scope.login = function (data){
        Auth.login(data).then(function(){
          Notifications.push('Welcome to SAMPLE TODO LISTS', 'success');
          $location.path('/');
        }, function (){
          Notifications.push('Login failed', 'danger');
        });
      };

      $scope.signup = function (data){
        Auth.signup(data).then(function(){
          $location.path('/');
        }, function (data){
          Notifications.push('Signup failed', 'danger');
        });
      };
  }]);