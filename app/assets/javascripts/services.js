angular.module('todoApp.services', [])
  .factory('Notifications',['$timeout', function($timeout) {
    var data = [],
      visibleTime = 3000,
      shift = function() {
        data.shift();
      };
    return {
      data: data,
      push: function(text, type) {
        type = type || 'info';
        var message = {
          text: text,
          type: type
        };
        data.push(message);
        $timeout(shift, visibleTime);
      },
    };
  }])
  .factory('Tasks', ['$resource', function($resource) {
    var factory = $resource('/tasks/:id',
      {
        id: '@id'
      },
      {
        save: {
          method: 'POST',
          url: '/projects/:project_id/tasks'
        },
        update: {
          method: 'PUT'
        }
      }
    );
    factory.priorities = ['low', 'normal', 'high'];
    // factory.priorities = { 0: 'low', 1: 'normal', 2: 'high'};
    return factory;
  }])
  .factory('Projects', ['$resource', function($resource) {
    return $resource('/projects/:id',
      {
        id: '@id'
      },
      {
        update: {
          method: 'PUT'
        }
      }
    );
  }])
  .factory('Debounce', ['$timeout', function ($timeout){
    function Debounce (){
      var timeout;
      this.debounce = function(callback, delay) {
        if (timeout) {
          $timeout.cancel(timeout);
        }
        timeout = $timeout(callback, delay);
      };
    }
    return Debounce;
  }])
  .factory('Auth', ['$http', '$q', function ($http, $q){
    var accessMap = {
        authorized: {
          check: function(user){
            return user;
          },
          redirect: '/login'
        },
        guest: {
          check: function(user){
            return !user;
          },
          redirect: '/'
        },
      },
      user,
      authPromise;
    return {
      isLoggedIn: function (){
        return !!user;
      },
      signup: function (userData){
        var $promise = $http.post('/users', userData);
        return $promise.then(function (data){
          user = data.data;
        });
      },
      login: function (credentials){
        var $promise = $http.post('/login', credentials);
        return $promise.then(function (data){
          user = data.data;
        });
      },
      logout: function (){
        var $promise = $http.post('/logout');
        return $promise.then(function (){
          user = undefined;
        });
      },
      checkAuth: function (){
        var deferred = $q.defer();
        if (!authPromise) {
          authPromise = $http.get('/authenticated');
          authPromise.then(function (data){
            user = data.data;
          }).finally(function (){
            deferred.resolve();
          });
        } else {
          deferred.resolve();
        }
        return deferred.promise;
      },
      handleAccesRule: function (rule){
        if (!accessMap[rule].check(user)) {
          return accessMap[rule].redirect;
        }
      }
    };
  }]);