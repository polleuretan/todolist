class TaskSerializer < ActiveModel::Serializer
  attributes :id, :title, :project_id, :priority, :deadline, :done, :created_at, :updated_at
end
