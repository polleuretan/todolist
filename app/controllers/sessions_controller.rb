class SessionsController < ApplicationController
  skip_before_action :authorized_only, :only => :create
  before_action :guest_only, :only => :create
  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      respond_with user, :location => nil, :status => 200
    else
      render :status => 422, :nothing => true
    end
  end
  def destroy
    session[:user_id] = nil
    render :status => 200, :nothing => true
  end
  def authenticated
    respond_with current_user, :location => nil, :status => 200
  end
end
