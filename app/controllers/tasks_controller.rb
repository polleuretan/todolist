class TasksController < ApplicationController
  before_action :load_project, :only => [ :create ]
  before_action :load_task, :only => [ :update, :destroy, :show ]

  def create
    respond_with Task.create(task_params.merge(:project => @project)), :location => nil
  end

  def show
    respond_with @task
  end

  def update
    respond_with Task.update(@task.id, task_params), :location => nil
  end

  def destroy
    respond_with Task.destroy(@task.id)
  end

  private
  def task_params
    params.permit(:title, :done, :priority, :deadline)
  end
  def load_project
    @project = Project.find_by_id(params[:project_id])
    owned_model_only(@project)
  end
  def load_task
    @task = Task.find_by_id(params[:id])
    owned_model_only(@task)
  end
end
