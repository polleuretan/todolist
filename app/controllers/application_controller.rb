class ApplicationController < ActionController::Base
  before_action :default_format
  before_action :authorized_only
  protect_from_forgery with: :exception

  respond_to :json

  protected
  def default_format
    unless request.format == 'application/json'
      redirect_to '/'
    end
  end
  def current_user
    @current_user ||= User.find_by_id(session[:user_id]) if session[:user_id]
  end
  def authorized_only
    render :status => 401, :nothing => true if current_user.nil?
  end
  def guest_only
    render :status => 400, :nothing => true unless current_user.nil?
  end
  def owned_model_only(model)
    if model.nil? || !model.owned_by?(current_user)
      render :status => 404, :nothing => true
    end
  end
end
