class UsersController < ApplicationController
  skip_before_action :authorized_only, :only => :create
  before_action :guest_only, :only => :create
  def create
    user = User.create(user_params)
    if user.valid?
      session[:user_id] = user.id;
    end
    respond_with user, :location => nil
  end

  private

  def user_params
    params.permit(:email, :password, :password_confirmation)
  end

end
