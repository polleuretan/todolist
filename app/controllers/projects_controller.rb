class ProjectsController < ApplicationController
  before_action :load_project, :only => [ :show, :update, :destroy ]
  def index
    respond_with Project.owned_by(current_user).all
  end

  def show
    respond_with @project
  end

  def create
    respond_with Project.create(project_params.merge(:user => current_user))
  end

  def update
    respond_with Project.update(@project.id, project_params)
  end

  def destroy
    respond_with Project.destroy(@project.id)
  end

  private
  def project_params
    params.permit(:title)
  end
  def load_project
    @project = Project.find_by_id(params[:id])
    owned_model_only(@project)
  end
end
