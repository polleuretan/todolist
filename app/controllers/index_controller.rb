class IndexController < ActionController::Base
  def index
    respond_to do |format|
      format.html { render :inline => '', :layout => 'application' }
    end
  end
end
